import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import store from './store'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import router from './router'
import EventBus from "@/js/EventBus";

Vue.config.productionTip = false

Vue.prototype.$apiUrl = 'https://santa-api.antony.tech';
// Vue.prototype.$apiUrl = 'http://santa.test';
axios.defaults.baseURL = Vue.prototype.$apiUrl;

Vue.prototype.$http = axios;

Vue.use(Buefy, {
  defaultIconPack: 'fas',
  defaultContainerElement: '#content'
});

// global events
Object.defineProperties(Vue.prototype, {
  $bus: {
    get: function() {
      return EventBus
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
