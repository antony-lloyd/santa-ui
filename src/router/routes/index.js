// import Home from "@views/Home";

export default [
    {
        path: '/',
        name: 'Home',
        component: () => import('../../views/Home.vue')
      },
      {
        path: '/santa',
        name: 'Santa',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../views/Santa.vue'),
        props: true,
        meta: {
          requireLoggedIn: true
        }
      }
]